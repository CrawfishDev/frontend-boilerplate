// All of this is just placeholder code. Delete the contents of this
// file and start from scratch.

import React from 'react'
import propTypes from 'prop-types'
import {render} from 'react-dom'
import {createStore} from 'redux'
import {Provider, connect} from 'react-redux'
import {css, styles} from 'linaria'


// Actions

const INCREMENT_COUNT = 'increment_count'
const PAUSE_COUNTER = 'pause_counter'
const START_COUNTER = 'start_counter'
const RESET_COUNT = 'reset_count'

const incrementCount = () => (
    {type: INCREMENT_COUNT}
)

const pauseCounter = () => (
    {type: PAUSE_COUNTER}
)

const startCounter = () => (
    {type: START_COUNTER}
)

const resetCount = () => (
    {type: RESET_COUNT}
)


// Reducer

const initialState = {
    count: 0,
    isCounting: true
}

const helloWorld = (state = initialState, action) => {
    switch(action.type) {
        case INCREMENT_COUNT:
            if (state.isCounting) return {...state, count: (state.count + 1)}
            else return state
        case PAUSE_COUNTER:
            return {...state, isCounting: false}
        case START_COUNTER:
            return {...state, isCounting: true}
        case RESET_COUNT:
            return {...state, count: 0}
        default:
            return state
    }
}


// Store

const store = createStore(helloWorld)


// Styles

const offWhite = '#EAEAEA'
const blue = '#C0D6DF'
const darkBlue = '#4F6D7A'
const orange = '#DD6E42'
const tan = '#E8DAB2'

const appStyles = css`
    color: ${orange}
    min-height: 100vh;
    background: ${blue};
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`

const pageHeaderStyles = css`
    font-size: 3rem;
`

const counterStyles = css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

const countStyles = css`
    font-size: 4rem;
    background: ${tan};
    border-radius: 50%;
    width: 10rem;
    height: 10rem;
    display: flex;
    justify-content: center;
    align-items: center;
    border: .5rem solid ${orange};
`

const buttonStyles = css`
    cursor: pointer;
    display: inline-block;
    width: 10rem;
    text-align: center;
    font-family: sans-serif;
    font-size: 1.5rem;
    padding: .75rem;
    margin: 1rem;
    margin-top: 2rem;
    border-radius: .5rem;
    background: ${darkBlue};
    color: ${offWhite};
`


// Components

const Count = ({count}) => <div {...styles(countStyles)}>{count}</div>
Count.propTypes = {
    count: propTypes.number.isRequired
}

const StartPauseButton = ({isCounting, startCounter, pauseCounter}) => <a
    {...styles(buttonStyles)}
    onClick={isCounting ? pauseCounter : startCounter}>
    {isCounting ? 'Pause' : 'Start'}
</a>

const ResetCountButton = ({resetCount}) => <a {...styles(buttonStyles)} onClick={resetCount}>
    Reset
</a>

const counter = ({isCounting, count, startCounter, pauseCounter, resetCount}) => <div {...styles(counterStyles)}>
    <Count count={count} />
    <div>
        <StartPauseButton
            isCounting={isCounting}
            startCounter={startCounter}
            pauseCounter={pauseCounter} />
        <ResetCountButton resetCount={resetCount} />
    </div>
</div>
const mapStateToProps = state => (
    {
        count: state.count,
        isCounting: state.isCounting
    }
)
const mapDispatchToProps = {
    startCounter,
    pauseCounter,
    resetCount
}
const Counter = connect(mapStateToProps, mapDispatchToProps)(counter)

const App = () => <div {...styles(appStyles)}>
    <h1 {...styles(pageHeaderStyles)}>Hello World</h1>
    <Counter />
</div>


// Make things happen

document.addEventListener('DOMContentLoaded', () => {
    const root = document.querySelector('#appRoot')
    render(
        <Provider store={store}>
            <App />
        </Provider>,
        root
    )
})

setInterval(() => {
    store.dispatch(incrementCount())
}, 1000)