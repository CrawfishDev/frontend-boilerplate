const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin')

const isDevelopment = (process.env.NODE_ENV || 'development') === 'development'

module.exports = {
    context: path.join(__dirname, 'src'),
    entry: './app.js',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'app.js'
    },
    plugins: [
        new HtmlWebpackPlugin({template: 'index.html'}),
        new HtmlWebpackIncludeAssetsPlugin({assets: 'styles.css', append: true}),
        new webpack.EnvironmentPlugin({NODE_ENV: 'development', DEBUG: false})
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            'env',
                            'react',
                            ['linaria/babel', {
                                'single': true,
                                'filename': 'styles.css',
                                'outDir': 'dist'
                            }]
                        ],
                        plugins: ['transform-object-rest-spread']
                    }
                }
            }
        ]
    },
    devtool: (isDevelopment ? 'eval-source-map' : false)
}