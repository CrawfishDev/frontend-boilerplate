# Frontend Boilerplate

This is our boilerplate code for frontend projects.

## What's Included?

- Webpack
- Babel
- React
- Redux
- Linaria

# TODO

- Testing
- Comments for webpack config